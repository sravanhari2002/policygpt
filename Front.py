import streamlit as st
from test import *
# Set page title
st.set_page_config(page_title="Policy GPT")

# Display title and description
st.title("Policy GPT")
st.write("Upload a PDF and ask Question")
uploaded_file = st.file_uploader('Choose your .pdf file (only one PDF at a time)', type="pdf")
if uploaded_file is not None:
   dataf=first(uploaded_file)
given_question = st.text_input("Ask Question: ", "")
submit = st.button("Submit")
if submit:
    st.write("",answer_question(dataf,question=given_question))